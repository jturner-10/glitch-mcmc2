#~~~~~~~PROGRAM TO REMOVE nan OR 0 OR too big ERRORS FROM mjd_f0_f1.dat~~~~~~~#

import matplotlib.pyplot as plt
import numpy as np
import os

#a, b, err, d, e, f, g = np.loadtxt("mjd_f0_f1.dat", unpack=True, usecols=[0, 1, 2, 3, 4, 5, 6])

#for i in range(0, len(err)):
#    if err[i] !=0 or err[i] != "-nan":
#        print(a[i], b[i], err[i], d[i], e[i], f[i], g[i])


if os.path.isfile("mjd_f0_f1_p.dat"):
    os.remove("mjd_f0_f1_p.dat")

with open("mjd_f0_f1.dat", 'r') as file:
    for line in file.readlines():
        fields = line.split()
        if float(fields[2]) != 0:
            if str(fields[2]) != '-nan':
                if float(fields[2]) < 1:
                    with open("mjd_f0_f1_p.dat",'a') as new_file:
                        new_file.write(line)
        

