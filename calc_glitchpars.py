#!/usr/bin/env python

from numpy import *
from uncertainties import ufloat

EPOCH = ufloat(55817.801764, 0.002205)
print(EPOCH)
F0 = ufloat(3.6991898513377035, 0.0000000005313513)
F1 = ufloat(-5.186505832311e-14, 3.643429557765e-17)
GLF0 = ufloat(2.06351548e-05, 8.73570023e-10)
GLF1 = ufloat(0, 0)

print "F0:", F0
print "F1:", F1
print "glitch epoch:", EPOCH
print ""
print "DF0", GLF0
print "DF1:", GLF1
print "DF0/F0:", (GLF0)/F0
print "DF1/F1:", (GLF1/F1)/1e-03, "milli units"

print "Total spin-up:", GLF0
print "Total fractional spin up:", (GLF0/F0)/1e-09, "nano units"

# delineate value from its uncertainty example
print("")
print("F0 without uncertainty: {}".format(F0.nominal_value))
print("F0 uncertainty only: {}".format(F0.std_dev))


