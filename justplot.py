#!/usr/bin/env python

#% matplotlib inline
#%config InlineBackend.figure_format = 'retina'

# Panel 1

from __future__ import division,print_function


from mpi4py import MPI
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()
print(size,rank)


import numpy as np
import glob
import matplotlib.pyplot as plt
import scipy.linalg as sl

import enterprise
from enterprise.pulsar import Pulsar
import enterprise.signals.parameter as parameter
from enterprise.signals import utils
from enterprise.signals import signal_base
from enterprise.signals import selections
from enterprise.signals.selections import Selection
from enterprise.signals import white_signals
from enterprise.signals import gp_signals
from enterprise.signals import deterministic_signals

from enterprise_extensions import models, model_utils

import scipy.stats as stats
import corner
from PTMCMCSampler.PTMCMCSampler import PTSampler as ptmcmc
import sys


psrname="B0531+21"

if rank==0:
    # Panel 7

    chain = np.loadtxt('chains/'+psrname+'/chain_1.0.txt')
    #pars = np.array(sorted(xs.keys()))
    pars = np.loadtxt('chains/'+psrname+'/pars.txt', dtype=np.unicode_)

    burn = 15000
    print("Burn=",burn)

    # Panel 8


    namedict = {
        "B0531+21_efac": "EFAC",
        "B0531+21_gl_GLF0D_1": r"$\Delta\nu_{d,1}$",
        "B0531+21_gl_GLF0_1": r"$\Delta\nu$",
        "B0531+21_gl_GLF1_1": r"$\Delta\dot{\nu}$",
        "B0531+21_gl_GLF2_2": r"$\Delta\ddot{\nu}$",
        "B0531+21_gl_GLTD_1": r"$\tau_{d,1}$",
        "B0531+21_gl_GLF0D_2": r"$\Delta\nu_{d,2}$",
        "B0531+21_gl_GLTD_2": r"$\tau_{d,2}$" ,
        "B0531+21_gl_GLF0D_3": r"$\Delta\nu_{d,3}$",
        "B0531+21_gl_GLTD_3": r"$\tau_{d,3}$",
        "B0531+21_gl_GLEP_1": r"t$_{g}$",
        "B0531+21_red_noise_gamma": r"$\gamma$",
        "B0531+21_red_noise_log10_A": r"LOG$_{10}$A"
    }

    transdict = {
        "B0531+21_efac": 1.0,
        "B0531+21_gl_GLF0D_1": 1e-06,
        "B0531+21_gl_GLF0_1": 1e-06,
        "B0531+21_gl_GLF1_1": 1e-12,
        "B0531+21_gl_GLF2_2": 1e-20,
        "B0531+21_gl_GLTD_1": 1.0,
        "B0531+21_gl_GLF0D_2": 1e-06,
        "B0531+21_gl_GLTD_2": 1.0,
        "B0531+21_gl_GLF0D_3": 1e-06,
        "B0531+21_gl_GLTD_3": 1.0,
        "B0531+21_gl_GLEP_1": 1.0,
        "B0531+21_red_noise_gamma": 1.0,
        "B0531+21_red_noise_log10_A": 1.0
    }

    for i in range(len(pars)):
        if pars[i] in namedict:
            print(namedict[pars[i]],np.mean(chain[burn:,i]) * transdict[pars[i]],np.std(chain[burn:,i])* transdict[pars[i]])

    # Panel 9

    print("pars = {}".format(pars))
    print(type(pars[0]))
    print(pars[0])
    size = len(pars)
    par2 = ["" for x in range(size)]
    for i in range(len(pars)):
        if pars[i] in namedict:
            par2[i]=namedict[pars[i]]
        else:
            par2[i]=pars[i]
        if "efac" in pars[i]:
            par2[i] = "EFAC"
        if "f0d" in pars[i]:
            par2[i] = "GLF0_D (uHz)"
        if "glf0" in pars[i]:
            par2[i] = "GLF0 (uHz)"
        if "glf1" in pars[i]:
            par2[i] = "GLF1 (pHz/s)"
        if "glf2" in pars[i]:
            par2[i] = "GLF2"
        if "td" in pars[i]:
            par2[i] = "T (days)"
        if "glep" in pars[i]:
            par2[i] = "EPOCH"
        if "td2" in pars[i]:
            par2[i] = "T2 (days)"
        if "f0d2" in pars[i]:
            par2[i] = "GLF0_D2 (uHz)"
        if "td3" in pars[i]:
            par2[i] = "T3 (days)"
        if "f0d3" in pars[i]:
            par2[i] = "GLF0_D3 (uHz)"


    corner.corner(chain[burn:,:len(pars)],labels=par2,smooth=True, fontsize=30)
    plt.savefig("corner.pdf", format='pdf', dpi=400)
    plt.show()

    #plt.plot(psr.toas,psr.residuals-glitch_recovery(psr.toas,f0,glep,np.mean(chain[burn:,1]),np.mean(chain[burn:,4]),np.mean(chain[burn:,2]),np.mean(chain[burn:,3])),'.')
    #plt.show()


