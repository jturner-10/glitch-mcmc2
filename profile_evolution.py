#!/usr/bin/env python

# import all our modules
import matplotlib.pyplot as plt
import numpy as np
from numpy import loadtxt 
from lmfit.models import GaussianModel
from math import sqrt, pi, exp
from scipy.optimize import curve_fit
import os, sys
from os import listdir
from os.path import isfile, join
from numpy import *
from uncertainties import ufloat
from time import sleep

#            All .it should be placed in a separate directory labelled by 'directory'
#       Program is being run in directory labelled 'current' where files will be created

directory = "/local/scratch/jturner/small_Crab_glitches/58470/work_glitch/test/"
current = "/local/scratch/jturner/small_Crab_glitches/58470/work_glitch/"

#function to check if parameter file exists and deletes it
def check_file():
    filepath = current + "params.txt"
    if os.path.exists(filepath):
        check = os.remove(filepath)
        print("params.txt has been removed")
    return None

#function that looks for and lists .it files in the directory 'test'
def list():
    itfiles = [itfile for itfile in listdir(directory) if isfile(join(directory, itfile))]
    return itfiles

def make_pdv(itfile):
    """
    Runs command to build .pdv files
    :param: itfile  Path to archive file
                    <type=str>
    return: output_filename Path to pdv file
                            <type=str>
    """
    input_file = directory + itfile
    filename = os.path.splitext(itfile)[0]
    output_filename = str(filename) + ".pdv"
    command = "pdv -t " + str(input_file) + " > " + str(output_filename)
    os.system(command)
    return output_filename

#function to get mjd of observation
def mjd_get(itfile):
    input_file = directory + itfile
    command = "vap -n -c mjd " + str(input_file)
    vap_out = os.popen(command).read()
    mjd = float(vap_out.strip().split()[1])
    return mjd

#function that finds the maximum flux bin
def find_max(power):
    maxbin = np.argmax(power)
    return maxbin

#function that rolls the profile so maxbin --> 256th bin (= 255th column)
def align(power, maxbin):
    lag = 256-maxbin
    aligned = np.roll(power, lag)
    return aligned

#function that normalises main peak to be 1000
def normalise(aligned):
    maxpower = np.max(aligned)
    normalised = aligned * (1000/maxpower)
    return normalised

#function that describes a gaussian
def gaussian(bins, A1, o1, c1, A2, o2, c2, A3, o3, c3):
    model = []
    for bin in bins:
        #model.append(((A1/(sqrt(2*pi)*o1))*exp(-(bin-c1)**2 / (2*o1**2))) + ((A2/(sqrt(2*pi)*o2))*exp(-(bin-c2)**2 / (2*o2**2))) + ((A3/(sqrt(2*pi)*o3))*exp(-(bin-c3)**2 / (2*o3**2))))
        model.append(((A1)*exp(-(bin-c1)**2 / (2*o1**2))) + ((A2)*exp(-(bin-c2)**2 / (2*o2**2))) + ((A3)*exp(-(bin-c3)**2 / (2*o3**2))))
    return model

#function to write out full file
def write_file(myfile, params):
    for paramss in params:
        myfile.write(str(paramss)+' ')
    myfile.write('\n')
    return myfile

#function to plot data and fits and save to png
def plotter(bins, normalised, fitdata, itfile, mjd):
    input_file = directory + itfile
    filename = os.path.splitext(itfile)[0]
    output_figure = str(filename) + ".png"
    fig, ax = plt.subplots()
    ax.plot(bins, normalised, 'k.')
    ax.plot(bins, fitdata, 'r-')
    ax.set_xlabel('Bin', fontname='serif')
    ax.set_ylabel('Normalised Flux', fontname='serif')
    ax.title.set_text(output_figure + ' ' + str(mjd))
    #plt.show()
    fig.savefig(str(output_figure))
    plt.close()

#function to delete the .pdv file
def remove_file(output_filename):
    removed = os.remove(output_filename)
    return removed

#function to build the pdf file
def build_pdf():
    command = "convert " + current + "*.png profiles.pdf"
    convert = os.system(command)
    return convert

#function to delete all pngs
def delete_pngs():
    for file in os.listdir(current):
        if file.endswith(".png"):
            os.remove(file)
            print("all .png files have successfully been removed")


def main():

    #check the parameter file exists, if so delete it, then open it
    check = check_file()
    myfile = open('params.txt', 'a') 
    #run function that makes the list
    itfiles = list()
    #run function that uses the list to make them all .pdv files
    #run function that does all the rest for each .pdv file
    for itfile in itfiles:
        print("working on itfile {}".format(itfile))
        output_filename = make_pdv(itfile)
        bins, power = np.loadtxt(output_filename, skiprows=1, unpack=True, usecols=[2,3])
        mjd = mjd_get(itfile)
        print("mjd is {}".format(mjd))
        maxbin = find_max(power)
        aligned = align(power, maxbin)
        normalised = normalise(aligned)
#        plt.plot(bin, normalised)
#        plt.show()
        guess = np.array([74, 10, 200, 1000, 15, 256, 556, 15, 670], dtype='float')
        pars, cov = curve_fit(gaussian, bins, normalised, guess)
        #get around error if curvefit stops iterating
        try:
            pars, cov = curve_fit(gaussian, bins, normalised, guess)
        except RuntimeError:
            print("Error: file has not been included due to fit issue")
            continue
#        print("fit parameters = {}".format(pars))
#        print("covariance matrix = {}".format(cov))
        fitdata = gaussian(bins, pars[0], pars[1], pars[2], pars[3], pars[4], pars[5], pars[6], pars[7], pars[8])       
        # Get precursor params
        # Height
        amp1 = pars[0]
        amp1e = np.sqrt(cov[0][0])

        #FWHM
        sigma1 = ufloat(pars[1], np.sqrt(cov[1][1]))
        conv = 2.355 * sigma1
        fwhm1 = conv.nominal_value 
        fwhm1e = conv.std_dev

        # Mean
        mean1 = pars[2]
        mean1e = np.sqrt(cov[2][2])

        #  Get main pulse params
        # Height
        amp2 = pars[3]
        amp2e = np.sqrt(cov[3][3])

        #FWHM
        sigma2 = ufloat(pars[4], np.sqrt(cov[4][4]))
        conv = 2.355 * sigma2
        fwhm2 = conv.nominal_value
        fwhm2e = conv.std_dev

        # Mean
        mean2 = pars[5]
        mean2e = np.sqrt(cov[5][5])

        # Get interpulse params
        # Height
        amp3 = pars[6]
        amp3e = np.sqrt(cov[6][6])

        #FWHM
        sigma3 = ufloat(pars[7], np.sqrt(cov[7][7]))
        conv = 2.355 * sigma3
        fwhm3 = conv.nominal_value
        fwhm3e = conv.std_dev

        # Mean
        mean3 = pars[8]
        mean3e = np.sqrt(cov[8][8])
       
#        print mjd, amp1, amp1e, fwhm1, fwhm1e, mean1, mean1e, amp2, amp2e, fwhm2, fwhm2e, mean2, mean2e, amp3, amp3e, fwhm3, fwhm3e, mean3, mean3e
        params = [mjd, amp1, amp1e, fwhm1, fwhm1e, mean1, mean1e, amp2, amp2e, fwhm2, fwhm2e, mean2, mean2e, amp3, amp3e, fwhm3, fwhm3e, mean3, mean3e]
        myfile = write_file(myfile, params)
        plotter(bins, normalised, fitdata, itfile, mjd)    
        removed = remove_file(output_filename)
        sleep(1)
    convert = build_pdf()
    delete_pngs()

main()



